﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace RemAss
{
  internal class Program
  {
    #region Static Fields

    private static readonly List<string> DefaultAssembliesStopWords = new List<string>
    {
      "sitecore",
      "microsoft",
      "apache",
      "telerik"
    };

    #endregion

    #region Methods

    private static bool IsDefaultAssembly(string fileName)
    {
      FileVersionInfo myFileVersionInfo = FileVersionInfo.GetVersionInfo(fileName);
      var lowercasedCopyRight = myFileVersionInfo.LegalCopyright;

      if (string.IsNullOrEmpty(lowercasedCopyRight))
      {
        return false;
      }

      lowercasedCopyRight = lowercasedCopyRight.ToLowerInvariant();

      return DefaultAssembliesStopWords.Any(sw => lowercasedCopyRight.Contains(sw));
    }

    private static bool IsManagedAssembly(string fileName)
    {
      using (Stream fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read))
      {
        using (BinaryReader binaryReader = new BinaryReader(fileStream))
        {
          if (fileStream.Length < 64)
          {
            return false;
          }

          //PE Header starts @ 0x3C (60). Its a 4 byte header.
          fileStream.Position = 0x3C;
          uint peHeaderPointer = binaryReader.ReadUInt32();
          if (peHeaderPointer == 0)
          {
            peHeaderPointer = 0x80;
          }

          // Ensure there is at least enough room for the following structures:
          //     24 byte PE Signature & Header
          //     28 byte Standard Fields         (24 bytes for PE32+)
          //     68 byte NT Fields               (88 bytes for PE32+)
          // >= 128 byte Data Dictionary Table
          if (peHeaderPointer > fileStream.Length - 256)
          {
            return false;
          }

          // Check the PE signature.  Should equal 'PE\0\0'.
          fileStream.Position = peHeaderPointer;
          uint peHeaderSignature = binaryReader.ReadUInt32();
          if (peHeaderSignature != 0x00004550)
          {
            return false;
          }

          // skip over the PEHeader fields
          fileStream.Position += 20;

          const ushort PE32 = 0x10b;
          const ushort PE32Plus = 0x20b;

          // Read PE magic number from Standard Fields to determine format.
          var peFormat = binaryReader.ReadUInt16();
          if (peFormat != PE32 && peFormat != PE32Plus)
          {
            return false;
          }

          // Read the 15th Data Dictionary RVA field which contains the CLI header RVA.
          // When this is non-zero then the file contains CLI data otherwise not.
          ushort dataDictionaryStart = (ushort)(peHeaderPointer + (peFormat == PE32 ? 232 : 248));
          fileStream.Position = dataDictionaryStart;

          uint cliHeaderRva = binaryReader.ReadUInt32();
          if (cliHeaderRva == 0)
          {
            return false;
          }

          return true;
        }
      }
    }

    private static int Main(string[] args)
    {
      if ((args.Length == 1 && args[0].Equals("/?", StringComparison.OrdinalIgnoreCase)) || args.Length == 0)
      {
        Console.WriteLine("Tool to remove the non-managed assemblies from the specified (or current) directory. Also it allows to remove default assemblies (when copyright contains either Sitecore, Apache, Telerik or Microsoft).\r\n\r\n Usage: \r\n" +
                          "\r\n RemUnmanaged filter [/rd]-- Remove all files by mask in current directory. E.g.: RemUnmanaged *.*" +
                          "\r\n RemUnmanaged fullPath\\[filter] [/rd]-- Remove all files by mask in the specified directory. E.g.: RemUnmanaged D:\\dir\\ or RemUnamanged D:\\dir\\*.*" +
                          "\r\n\r\n /rd flag - remove \"default\" assemblies.");
        //Print help
        return 0;
      }

      var argsList = new List<string>(args);
      bool removeDefault = false;

      const string remDefKey = "/rd";
      if (argsList.Contains(remDefKey))
      {
        removeDefault = true;
        argsList.Remove(remDefKey);
      }

      if (argsList.Count == 0)
      {
        Console.WriteLine("Wrong arguments. Directory path is expected.");
        return 1;
      }

      string fileMask;
      string workingDirectory;

      var arg0 = args[0];

      bool rootedPath = false;

      if (Path.IsPathRooted(arg0))
      {
        workingDirectory = Path.GetDirectoryName(arg0);
        fileMask = Path.GetFileName(arg0);
        rootedPath = true;
      }
      else
      {
        fileMask = arg0;
        workingDirectory = Directory.GetCurrentDirectory();
      }

      if (string.IsNullOrEmpty(fileMask))
      {
        fileMask = "*.*";
      }
      else if (!fileMask.Contains(".") && !arg0.EndsWith("\\") && rootedPath)
      {
        Console.WriteLine("WARNING. File mask doesn't contain dots. Likely, directory path doesn't end with slash (should be. D:\\dir\\)");
        Console.WriteLine();
      }

      Console.WriteLine("Task INFO.{0}Dir: {1}{0}Filter: {2}{0}Remove default:{3}", Environment.NewLine, workingDirectory, fileMask, removeDefault);

      if (!Directory.Exists(workingDirectory))
      {
        Console.WriteLine("The " + workingDirectory + " directory doesn't exists.");
        return 1;
      }

      try
      {
        string[] dllFiles = Directory.GetFiles(workingDirectory, fileMask, SearchOption.AllDirectories);

        foreach (string dllFile in dllFiles)
        {
          if (!IsManagedAssembly(dllFile))
          {
            File.Delete(dllFile);
            Console.WriteLine("Deleted unmanaged: " + Path.GetFileName(dllFile));
          }
          else if (removeDefault && IsDefaultAssembly(dllFile))
          {
            File.Delete(dllFile);
            Console.WriteLine("Deleted default: " + Path.GetFileName(dllFile));
          }
        }
      }
      catch (Exception ex)
      {
        Console.WriteLine(ex.ToString());
      }

      return 0;
    }

    #endregion
  }
}