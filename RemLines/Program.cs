﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace RemLines
{
  internal class Program
  {
    #region Static Fields

    private static string[] KnownFlags =
    {
      "/o",
      "/d"
    };

    #endregion

    #region Methods

    private static string[] GetFilesToProcess(string specifiedFileName, bool searchRecursively, out bool wildcardSearch)
    {
      //non wildcard search
      if (!specifiedFileName.Contains("*") && !specifiedFileName.Contains("?"))
      {
        wildcardSearch = false;
        var fullFilePath = specifiedFileName;
        if (!Path.IsPathRooted(specifiedFileName))
        {
          fullFilePath = Path.Combine(Directory.GetCurrentDirectory(), specifiedFileName);
        }


        return new[]
        {
          fullFilePath
        };
      }

      // this is wildcard search
      wildcardSearch = true;

      string currentDirectory;
      string searchPattern;
      if (Path.IsPathRooted(specifiedFileName))
      {
        currentDirectory = Path.GetDirectoryName(specifiedFileName);
        searchPattern = Path.GetFileName(specifiedFileName);
      }
      else
      {
        currentDirectory = Directory.GetCurrentDirectory();
        searchPattern = specifiedFileName;
      }

      return Directory.GetFiles(currentDirectory, searchPattern, searchRecursively ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly);
    }

    private static void Main(string[] args)
    {
      if (args.Length < 2)
      {
        Console.WriteLine("The args are specified wrongly. Format is \"string to search 1\" .. \"string to search N\" fileNameOrPath");
        return;
      }

      try
      {
        bool overrideFile = args.Any(a => a.Equals("/o", StringComparison.OrdinalIgnoreCase));
        bool deepScan = args.Any(a => a.Equals("/d", StringComparison.OrdinalIgnoreCase));

        //Properly handle wildcards
        bool wildcardSearch;
        string[] filesToProcess = GetFilesToProcess(args[args.Length - 1], deepScan, out wildcardSearch);

        if (!wildcardSearch && !File.Exists(filesToProcess[0]))
        {
          Console.Write("File '{0}' was not found!", File.Exists(filesToProcess[0]));
          return;
        }

        var markers = args.Take(args.Length - 1).Where(m => !KnownFlags.Contains(m)).Select(m => m.ToLowerInvariant()).ToList();

        foreach (string fileToProcess in filesToProcess)
        {
          string currentSourceDir = Path.GetDirectoryName(fileToProcess);
          string currentSourceExt = Path.GetExtension(fileToProcess);
          string currentOutputFilePath = Path.Combine(currentSourceDir, fileToProcess + " - clean" + currentSourceExt);
          long linesRemoved = 0;

          using (var sr = new StreamReader(fileToProcess))
          {
            using (var cw = new StreamWriter(currentOutputFilePath))
            {
              Console.WriteLine("Processing file: " + fileToProcess);
              while (!sr.EndOfStream)
              {
                string line = sr.ReadLine();
                bool shouldBeStripped = markers.Any(m => line.ToLowerInvariant().Contains(m));
                if (!shouldBeStripped)
                {
                  cw.WriteLine(line);
                }
                else
                {
                  linesRemoved++;
                }
              }
            }
          }

          if (overrideFile)
          {
            File.Delete(fileToProcess);
            File.Move(currentOutputFilePath, fileToProcess);
            Console.WriteLine("Original file was overridden");
          }

          Console.WriteLine("File processed. Lines removed: " + linesRemoved);
        }
      }
      catch (Exception ex)
      {
        Console.WriteLine("Unhandled exception.");
        Console.WriteLine();
        Console.WriteLine();
        Console.WriteLine(ex.ToString());
      }
    }

    #endregion
  }
}